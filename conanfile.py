from conans import ConanFile, CMake, tools, VisualStudioBuildEnvironment
import os

linux_makefile_patch = """
\tln -s libbz2.so.1.0.6 libbz2.so.1.0
\tln -s libbz2.so.1.0.6 libbz2.so
"""

windows_shared_link_patch = """
    lib /out:libbz2.lib $(OBJS)
    link /DLL /DEF:bz2.def /OUT:bz2.dll $(OBJS)
"""

windows_debug_build_patch = """
CFLAGS= -DWIN32 -MDd -Od -D_FILE_OFFSET_BITS=64 -nologo
"""

windows_cflags_line = "CFLAGS= -DWIN32 -MD -Ox -D_FILE_OFFSET_BITS=64 -nologo"

                    
class Bzip2Conan(ConanFile):
    name = "bzip2"
    version = "1.0.6"
    license = "<Put the package license here>"
    url = "<Package recipe repository url here, for issues about the package>"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources=["*.tar.gz",'bz2.def']
    exports=['FindBZip2.cmake',
             "SelectLibraryConfigurations.cmake",
             "FindPackageHandleStandardArgs.cmake",
             "FindPackageMessage.cmake",
             "CheckSymbolExists.cmake",
             "CMakeParseArguments.cmake",
             "CMakePushCheckState.cmake", ]

    # -------------------------------------------------------------------------
    # Unpacking and patching the sources
    # -------------------------------------------------------------------------
    def source(self):
        if self.settings.build_type=="Debug":
            self.output.info("Patching sources for Debug build")
        elif self.settings.build_type=="Release":
            self.output.info("Patching sources for Release build")

        archive_file = 'bzip2-1.0.6.tar.gz'
        tools.unzip(archive_file)
        
        if self.settings.os=='Windows':
            if self.options.shared:
                tools.replace_in_file("bzip2-1.0.6/makefile.msc",
                    "lib /out:libbz2.lib $(OBJS)", windows_shared_link_patch)
                self.run('copy bz2.def bzip2-1.0.6')

            if self.settings.build_type=="Debug":
                tools.replace_in_file("bzip2-1.0.6/makefile.msc",
                    windows_cflags_line,windows_debug_build_patch)    
                        

        elif self.settings.os=='Linux':
            tools.replace_in_file("bzip2-1.0.6/Makefile-libbz2_so",
                    "ln -s libbz2.so.1.0.6 libbz2.so.1.0",linux_makefile_patch)

            if self.settings.build_type=="Debug":
                tools.replace_in_file("bzip2-1.0.6/Makefile-libbz2_so",
                        "CFLAGS=-fpic -fPIC -Wall -Winline -O2 -g $(BIGFILES)",
                        "CFLAGS=-fpic -fPIC -Wall -Winline -O0 -g $(BIGFILES)")
                tools.replace_in_file("bzip2-1.0.6/Makefile",
                        "CFLAGS=-Wall -Winline -O2 -g $(BIGFILES)",
                        "CFLAGS=-Wall -Winline -O0 -g $(BIGFILES)")

    # -------------------------------------------------------------------------
    # Build the code
    # -------------------------------------------------------------------------
    def build(self):

        if self.settings.os=='Windows' and self.settings.compiler=='Visual Studio':
            build_env = VisualStudioBuildEnvironment(self)
            vcvars = tools.vcvars_command(self.settings)
            with tools.environment_append(build_env.vars):
                self.run('cd bzip2-1.0.6 && %s && nmake /f makefile.msc all' %(vcvars))
        elif self.settings.os=='Linux':
            if self.options.shared:
                self.run('cd bzip2-1.0.6 && make -f Makefile-libbz2_so')
            else:
                self.run('cd bzip2-1.0.6 && make')
                self.run('cd bzip2-1.0.6 && make install PREFIX=%s' %(self.package_folder))


    # -------------------------------------------------------------------------
    # Installing binaries and required CMake files
    # -------------------------------------------------------------------------
    def package(self):
        self.copy("*.h", dst="include", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.so.*", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy('FindBZip2.cmake',".",".")
        self.copy("SelectLibraryConfigurations.cmake",".",".")
        self.copy("FindPackageHandleStandardArgs.cmake",".",".")
        self.copy("FindPackageMessage.cmake",".",".")
        self.copy("CheckSymbolExists.cmake",".",".")
        self.copy("CMakeParseArguments.cmake",".",".")
        self.copy("CMakePushCheckState.cmake",".",".")

    # -------------------------------------------------------------------------
    # return packag info
    # -------------------------------------------------------------------------
    def package_info(self):
        self.cpp_info.libs = ["bzip2"]
